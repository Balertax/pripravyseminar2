﻿using System;
using Gtk;
using System.IO;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnLoadClicked(object sender, EventArgs e)
    {
        if (this.filePath.Text.Length == 0)
        {
            MessageDialog dialog = new MessageDialog(
                this, DialogFlags.DestroyWithParent, MessageType.Question,
                ButtonsType.YesNo, "Opravdu načíst prázdný soubor?");
            int response = dialog.Run();
            Console.WriteLine(response);
            dialog.Destroy();
        }
        else
        {
            StreamReader sr = new StreamReader(filePath.Text);
            textContent.Buffer.Text = sr.ReadToEnd();
        }
    }

    protected void OnSaveClicked(object sender, EventArgs e)
    {
        StreamWriter sw = new StreamWriter(filePath.Text);
        sw.Write(textContent.Buffer.Text);
        sw.Close();
    }
}
