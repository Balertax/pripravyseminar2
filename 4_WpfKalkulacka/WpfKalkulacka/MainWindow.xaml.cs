﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKalkulacka
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MainGrid.RowDefinitions.Add(new RowDefinition());
            var btn = new Button() { Content = "Dynamicky pridane tlacitko." };
            MainGrid.Children.Add(btn);
            // Takhle to vola XAML
            Grid.SetRow(btn, 5);
            Grid.SetColumn(btn, 0);
            Grid.SetColumnSpan(btn, 3);
            // Nasleduje nastaveni stejnych vlastnosti pres setValue. Nastaveni nahore jen vola metody dole.
            // Odtud je lepe videt koncept DependencyProperty (hash tabulka libovolnych vlastnosti)
            //btn.SetValue(Grid.RowProperty, 5);
            //btn.SetValue(Grid.ColumnProperty, 0);
            //btn.SetValue(Grid.ColumnSpanProperty, 3);
        }
    }
}
