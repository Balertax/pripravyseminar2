﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _2_WpfTextEdit
{
    /// <summary>
    /// Interakční logika pro ConfirmDialog.xaml
    /// </summary>
    public partial class ConfirmDialog : Window
    {
        private string textMessage = null;

        public ConfirmDialog()
        {
            InitializeComponent();
        }

        public ConfirmDialog(string message):this()
        {
            textMessage = message;
        }

        public ConfirmDialog(string message, string title) : this(message)
        {
            Title = title;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (textMessage != null) Message.Text = textMessage;
        }
    }
}
