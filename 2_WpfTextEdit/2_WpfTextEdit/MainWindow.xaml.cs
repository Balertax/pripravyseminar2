﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace _2_WpfTextEdit
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Opravdu uložit soubor?", "Opravdu?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (r == MessageBoxResult.Yes)
            {
                StreamWriter sw = new StreamWriter(FileName.Text);
                sw.Write(TextContent.Text);
                sw.Close();
            }
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            StreamReader sr = new StreamReader(FileName.Text);
            TextContent.Text = sr.ReadToEnd();
            sr.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Opravdu?", "Opravdu?", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }
    }
}
