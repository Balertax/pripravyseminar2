﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _5_WpfDynamicke
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int Rows = 2;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddColumn_Click(object sender, RoutedEventArgs e)
        {
            // nove tlacitko ulozim do pomocne promenne btn1
            var btn1 = new Button() { Content = (Rows * 2).ToString() };
            // nastavim event po kliknuti
            btn1.Click += GridButton_Click;
            // nastavim radek a sloupec v Gridu. Je to to stejne jako
            // btn1.SetValue(Grid.RowProperty, Rows);
            // btn1.SetValue(Grid.ColumnProperty, 0);
            Grid.SetRow(btn1, Rows);
            Grid.SetColumn(btn1, 0);
            // Pridam btn1 do gridu na poradi nastavovani tlacitka a pridavani do gridu nezalezi
            CentralGrid.Children.Add(btn1);
            // To stejne pro druhe tlacitko
            var btn2 = new Button() { Content = (Rows * 2 + 1).ToString() };
            btn2.Click += GridButton_Click;
            Grid.SetRow(btn2, Rows);
            Grid.SetColumn(btn2, 1);
            CentralGrid.Children.Add(btn2);
            // Nesmim zapomenout na pridani noveho radku.
            CentralGrid.RowDefinitions.Add(new RowDefinition());
            // A zapamatuju si, ze mam o radek vic.
            // mohl bych se misto toho vykaslat na rucni pocitani radku v promenne Rows a ziskat pocet radku z
            // CentralGrid.RowDefinitions.Count;
            Rows++;
        }


        private void GridButton_Click(object sender, RoutedEventArgs e)
        {
            // Pretypuju toho na koho bylo kliknuto na tlacitko (jsem si jisty, ze jinemu objektu jsem reakce na udalosti touto funkci nedaval)
            Button btn = (Button)sender;
            // Do uvodniho popisku dam text co je napsany na tomto tlacitku.
            IntroText.Text += btn.Content;
        }
    }
}
